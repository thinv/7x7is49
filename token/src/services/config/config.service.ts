export class ConfigService {
  private readonly envConfig: { [key: string]: any } = null;

  constructor() {
    this.envConfig = {
      broker: process.env.HOST_REDIS,
    };
  }

  get(key: string): any {
    return this.envConfig[key];
  }
}
