import { Transport } from '@nestjs/microservices';
export class ConfigService {
  private readonly envConfig: { [key: string]: any } = null;

  constructor() {
    
    const BROKER = {
      options: {
        url: process.env.HOST_REDIS
      },
      transport: Transport.REDIS,
    };
    this.envConfig = {};
    this.envConfig.port = process.env.API_GATEWAY_PORT;
    this.envConfig.tokenService = BROKER;
    this.envConfig.userService = BROKER;
  }

  get(key: string): any {
    return this.envConfig[key];
  }
}