import { NestFactory } from '@nestjs/core';
import { UserModule } from './user.module';
import { Transport, RedisOptions } from '@nestjs/microservices';

import { ConfigService } from './services/config/config.service';

async function bootstrap() {
  const app = await NestFactory.create(UserModule);
  app.enableCors();
  app.connectMicroservice({
    transport: Transport.REDIS,
    options: {
      url: (new ConfigService()).get('broker'),
    },
  } as RedisOptions);

  await app.startAllMicroservicesAsync();
  await app.listen((new ConfigService()).get('httpPort'));
}
bootstrap();
